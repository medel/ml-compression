#!/bin/sh

sudo apt update
sudo apt install -y git npm python3-numpy mesa-common-dev libegl1-mesa-dev libgles2-mesa-dev cmake make g++ libdrm-dev wget zip pkg-config libwayland-dev

wget -O artifacts.zip https://gitlab.collabora.com/medel/ml-compression/-/jobs/179330/artifacts/download
unzip artifacts

sudo mv out/libtensorflowlite.so out/libtensorflowlite_gpu_delegate.so /usr/lib

git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow
git checkout 2.9.0
cd ..
sudo ln -s /home/user/tensorflow/tensorflow /usr/local/include

git clone https://github.com/google/flatbuffers.git
cd flatbuffers
git checkout v1.12.0
mkdir build
cd build
cmake -DFLATBUFFERS_BUILD_TESTS=OFF ..
make
sudo make install

git clone https://github.com/jpc/tflite_gles_app.git
cd tflite_gles_app/gl2facemesh
make -j2 TARGET_ENV=wayland TFLITE_DELEGATE=GPU_DELEGATEV2
